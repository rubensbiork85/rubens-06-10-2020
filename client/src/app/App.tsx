import React from 'react';
import { ToastProvider } from 'react-toast-notifications';
import { ImageProvider } from '../context';
import Container from '../page/container';
import './App.css';

function App() {
  return (
    <div className="App">
      <ToastProvider>
        <ImageProvider>
          <Container />
        </ImageProvider>
      </ToastProvider>
    </div>
  );
}

export default App;

import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';

configure({ adapter: new Adapter() });

import Card from './index';

import { Container, Title, Footer, Size, Button } from './styled';

const image = {
  name: 'test',
  size: 1232132,
  url: 'www.test.com',
  onDelete: () => {},
};

describe('<Card />', () => {
  it('should render the styled elements and <Card />', () => {
    const component = shallow(
      <Card name={image.name} size={image.size} url={image.url} />,
    );

    expect(component.find(Container).exists()).toEqual(true);
    expect(component.find(Title).exists()).toEqual(true);
    expect(component.find(Footer).exists()).toEqual(true);
    expect(component.find(Size).exists()).toEqual(true);
    expect(component.find(Button).exists()).toEqual(true);
  });
});

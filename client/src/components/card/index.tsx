import React, { memo } from 'react';
import { Container, Title, Footer, Size, Button } from './styled';

import { bytesToSize } from '../../utils';

interface CardProps {
  name: string;
  size: number;
  url: string;
  onDelete?: (name: string) => void;
}

const Card: React.FC<CardProps> = ({ name, size, url, onDelete }) => {
  console.log('Card render:');
  const handleOnDelete = (
    e: React.MouseEvent<HTMLButtonElement>,
    name: string,
  ) => {
    e.preventDefault();
    if (onDelete) {
      onDelete(name);
    }
  };

  return (
    <Container href={url}>
      <Title>{name}</Title>
      <Footer>
        <Size>{bytesToSize(size)}</Size>
        <Button
          onClick={(e: React.MouseEvent<HTMLButtonElement>) =>
            handleOnDelete(e, name)
          }
        >
          delete
        </Button>
      </Footer>
    </Container>
  );
};

export default memo(Card);

import styled from 'styled-components';

export const Container = styled.a.attrs({
  target: '_blank',
  rel: 'noopener noreferrer',
})`
  display: flex;
  flex: 0 1 30%;
  flex-direction: column;
  justify-content: space-between;
  border: 0.8px groove black;
  min-width: 230px;
  height: 100px;
  color: inherit;
  text-decoration: inherit;
  padding: 20px;
  margin: 0 20px 20px 0;
  box-sizing: border-box;

  &:hover {
    background-color: lightgrey;
  }
`;

export const Title = styled.span`
  text-align: left;
  font-size: 1.2em;
  width: 200px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Size = styled.span``;

export const Button = styled.button`
  border: 1px solid black;
  background-color: #bfbfff;
  padding: 2px 20px;
  cursor: pointer;
`;

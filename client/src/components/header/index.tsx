import React from 'react';
import { Wrapper, Documents, Total } from './styled';

interface HeaderProps {
  numDocs: number;
  totalSize: string;
}

const Header: React.FC<HeaderProps> = ({ numDocs, totalSize }) => {
  return (
    <Wrapper>
      <Documents>{numDocs} documents</Documents>
      <Total>Total size: {totalSize}</Total>
    </Wrapper>
  );
};

export default Header;

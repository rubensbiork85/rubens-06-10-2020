import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 40px 57px 40px 0;
  height: 20px;
  align-items: baseline;

  @media (max-width: 900px) {
    isplay: flex;
    flex-direction: column;
    align-items: start;
  }
`;

export const Documents = styled.div`
  font-size: 1.7em;
  display: flex;
  width: 200px;
`;

export const Total = styled.div`
  font-size: 1.125em;
`;

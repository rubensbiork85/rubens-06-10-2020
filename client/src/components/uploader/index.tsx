import React, { useRef, useContext, InputHTMLAttributes } from 'react';
import { ImageContext } from '../../context';
import { isAllowedMimeType, isAllowedSize } from '../../utils';

import {
  Wrapper,
  SearchInput,
  UploadInput,
  InputFileWrapper,
  Label,
} from './styled';

const Uploader: React.FC = () => {
  const [, actions] = useContext(ImageContext);
  const fileInput = useRef<HTMLInputElement>(null);

  const handleOnFileChange = () => {
    const files = fileInput?.current?.files;
    if (files && files[0]) {
      if (!isAllowedSize(files[0].size)) {
        actions.error('File is too big. It must be under 10mb');
        return;
      }
      if (!isAllowedMimeType(files[0].type)) {
        actions.error('File type not allowed. It must be .jpg or .png');
        return;
      }
      actions.uploadImage(files[0]);
    }
  };

  const handleOnSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    actions.searchImages(e.target.value);
  };

  return (
    <Wrapper>
      <SearchInput
        type="text"
        name="search"
        onChange={handleOnSearch}
        placeholder="Search documents..."
      />
      <InputFileWrapper>
        <Label>UPLOAD</Label>
        <UploadInput
          type="file"
          name="file"
          id="files"
          ref={fileInput}
          onChange={handleOnFileChange}
        />
      </InputFileWrapper>
    </Wrapper>
  );
};

export default Uploader;

import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;

  @media (max-width: 900px) {
    display: flex;
    flex-direction: column;
    align-items: start;
  }
`;

export const SearchInput = styled.input.attrs({
  maxLength: 20,
})`
  height: 25px;
  width: 30%;
  min-width: 220px;
  background-color: #efefef;
  border: solid 1px black;

  &:hover,
  &:focus,
  &:active {
    outline: none;
  }

  @media (max-width: 900px) {
    margin-top: 20px;
    order: 1;
  }
`;

export const UploadInput = styled.input.attrs({
  type: 'file',
  accept: 'image/jpeg, image/png',
  multiple: false,
})`
  overflow: hidden;
  opacity: 0;
  cursor: pointer;
`;

export const InputFileWrapper = styled.div`
  display: flex;
  position: relative;
`;

export const Label = styled.div`
  font-size: 1em;
  border: 1px solid black;
  background-color: #bfbfff;
  padding: 5px 40px;

  position: absolute;
  left: 60px;

  @media (max-width: 900px) {
    left: 0;
    min-width: 144px;
  }
`;

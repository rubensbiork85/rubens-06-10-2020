export const ALLOWED_EXTENSIONS = ['.png', '.jpg'];
export const ALLOWED_MIMETYPES = ['image/png', 'image/jpeg'];
export const ALLOWED_SIZE = 10 * 1024 * 1024; // limiting files size to 10 MB

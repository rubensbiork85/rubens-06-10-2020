import axios from 'axios';
import sanitize from 'sanitize-filename';

const fetchImages = async (dispatch) => {
  try {
    dispatch({ type: 'loading', loading: true });
    const resp = await axios({
      method: 'get',
      url: '/getImages',
    });
    dispatch({
      type: 'getImages',
      images: resp.data.images,
      loading: false,
    });
    dispatch({ type: 'loading', loading: false });
  } catch (error) {
    dispatch({
      type: 'error',
      message: `Error fechting images.`,
    });
  }
};

function createActions(dispatch) {
  return {
    uploadImage: async (file) => {
      try {
        let fileData = new FormData();
        const filename = sanitize(file.name);
        //todo sanitaze name
        fileData.set('file', file, `${filename}`);
        await axios({
          method: 'post',
          url: '/uploadImage',
          data: fileData,
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        });
        dispatch({ type: 'success', message: 'File uploaded' });
      } catch (error) {
        dispatch({
          type: 'error',
          message: `Error uploading: ${sanitize(file.name)}`,
        });
      }

      //fecth images after uploading a new one
      fetchImages(dispatch);
    },

    //get all images
    getImages: async () => {
      fetchImages(dispatch);
    },

    //delete a image
    deleteImage: async (name) => {
      try {
        await axios({
          method: 'delete',
          url: `/deleteImage`,
          params: { filename: sanitize(name) },
        });
        dispatch({ type: 'deleteImage', name });
      } catch (error) {
        dispatch({
          type: 'error',
          message: `Error trying to delete: ${sanitize(name)}`,
        });
      }
    },

    searchImages: (name) => {
      dispatch({ type: 'searchImages', name });
    },

    error: (message) => {
      dispatch({ type: 'error', message });
    },

    resetError: () => {
      dispatch({ type: 'resetError' });
    },
    resetSuccess: () => {
      dispatch({ type: 'resetSuccess' });
    },
  };
}

export default createActions;

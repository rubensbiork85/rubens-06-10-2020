import React, { createContext, useReducer, useEffect } from 'react';
import createActions from './createActions';

import reducer, { initialState } from './reducer';
import { useToasts } from 'react-toast-notifications';

export const ImageContext = createContext();

export const ImageProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const actions = createActions(dispatch);
  const { addToast } = useToasts();

  useEffect(() => {
    actions.getImages();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  //handle general errors
  useEffect(() => {
    if (state.error) {
      addToast(state.error, {
        appearance: 'error',
        autoDismiss: true,
      });
      actions.resetError();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.error]);

  // //handle successful upload
  useEffect(() => {
    if (state.success) {
      addToast(state.success, {
        appearance: 'success',
        autoDismiss: true,
      });
      actions.resetSuccess();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.success]);

  return (
    <ImageContext.Provider value={[state, actions]}>
      {children}
    </ImageContext.Provider>
  );
};

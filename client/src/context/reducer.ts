import { IState, IImage } from '../types';
export const initialState = {
  images: [],
  searchedList: [],
  searchText: '',
  error: '',
  success: '',
  loading: false,
};

interface ActionLoading {
  type: string;
  loading: boolean;
}

interface ActionImage {
  type: string;
  images: IImage[];
}

interface ActionDelete {
  type: string;
  name: string;
}

interface ActionError {
  type: string;
  message: string;
}

interface ActionSuccess {
  type: string;
  name: string;
}

type Action = ActionLoading &
  ActionImage &
  ActionDelete &
  ActionError &
  ActionSuccess;

function reducer(state: IState, action: Action) {
  switch (action.type) {
    case 'loading':
      return { ...state, loading: action.loading };
    case 'getImages':
      return {
        ...state,
        searchedList: action.images,
        images: action.images,
      };
    case 'deleteImage': {
      //remove deleted element from images
      const newImageList = state.images.filter(
        (img) => img.name !== action.name,
      );
      //apply search to the new images array
      const newSearchList = newImageList.filter((img) =>
        img.name.toLowerCase().includes(state.searchText),
      );
      return {
        ...state,
        searchedList: newSearchList,
        images: newImageList,
      };
    }
    case 'searchImages':
      return {
        ...state,
        searchText: action.name.toLowerCase(),
        searchedList: state.images.filter((img) =>
          img.name.toLowerCase().includes(action.name.toLowerCase()),
        ),
      };

    //handle state of the messages
    case 'error':
      return {
        ...state,
        error: action.message,
      };
    case 'resetError':
      return {
        ...state,
        error: '',
      };
    case 'success':
      return {
        ...state,
        success: action.message,
      };
    case 'resetSuccess':
      return {
        ...state,
        success: '',
      };
    default:
      return state;
  }
}

export default reducer;

import React, { useContext } from 'react';
import { ImageContext } from '../../context';
import Main from '../main';
import { IContext } from '../../types';

const Container: React.FC = () => {
  const [data, actions] = useContext<IContext[]>(ImageContext);
  const imageList = data.searchedList || [];

  return (
    <Main
      deleteImage={actions.deleteImage}
      imageList={imageList}
      loading={data.loading}
    />
  );
};

export default Container;

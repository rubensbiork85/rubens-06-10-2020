import React, { memo } from 'react';
import { Wrapper, Content, CardWrapper } from './styled';
import Uploader from '../../components/uploader';
import Header from '../../components/header';
import Card from '../../components/card';
import { totalSize } from '../../utils';
import { IImage } from '../../types';

interface Props {
  deleteImage: (name: string) => void;
  imageList: IImage[];
  loading: boolean;
}

const Main: React.FC<Props> = ({ deleteImage, loading, imageList }) => {
  console.log('loading:', loading);
  return (
    <Wrapper>
      <Content>
        <Uploader />
        <Header
          numDocs={imageList.length || 0}
          totalSize={totalSize(imageList)}
        />
        <CardWrapper>
          {loading ? (
            <div>My nice animated spinner...</div>
          ) : (
            imageList.map((image) => (
              <Card
                key={image.id}
                name={image.name}
                size={image.size}
                url={image.url}
                onDelete={() => deleteImage(image.name)}
              />
            ))
          )}
        </CardWrapper>
      </Content>
    </Wrapper>
  );
};

export default memo(Main);

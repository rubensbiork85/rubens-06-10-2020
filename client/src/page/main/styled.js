import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 5rem;
  align-items: center;
  justify-content: center;
`;

export const Content = styled.div`
  max-width: 960px;
  width: 100%;
`;

export const CardWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

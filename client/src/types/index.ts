export interface IImage {
  id: string;
  name: string;
  size: number;
  mimeType: string;
  url: string;
}

export interface IState {
  images: IImage[];
  searchedList: IImage[];
  searchText: string;
  error: string;
  success: string;
  loading: boolean;
}

export interface IContext extends IState {
  getImages: () => void;
  resetError: () => void;
  resetSuccess: () => void;
  deleteImage: (imageName: string) => void;
}

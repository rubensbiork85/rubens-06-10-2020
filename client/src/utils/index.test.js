import { totalSize, isAllowedExtension, isAllowedMimeType } from './';
const data = {
  images: [
    {
      id: '1591798007357346',
      name: '2CMS_Creative_164657191_Kingfisher.jpg',
      size: 221569,
      mimeType: 'image/jpeg',
      url:
        'https://firebasestorage.googleapis.com/v0/b/rubens-test-1f87d.appspot.com/o/2CMS_Creative_164657191_Kingfisher.jpg?alt=media&token=f413850c-37d8-4a8e-87c7-ba8d5a1a2057',
    },
    {
      id: '1591798007101873',
      name: 'XVM17e6e88a-66df-11e7-a206-95aa27b386e9.jpg',
      size: 81992,
      mimeType: 'image/jpeg',
      url:
        'https://firebasestorage.googleapis.com/v0/b/rubens-test-1f87d.appspot.com/o/XVM17e6e88a-66df-11e7-a206-95aa27b386e9.jpg?alt=media&token=6a471b6e-5143-4f1e-906c-dcdcef926535',
    },
    {
      id: '1591798629018832',
      name: 'buzz2.jpg',
      size: 220523,
      mimeType: 'image/jpeg',
      url:
        'https://firebasestorage.googleapis.com/v0/b/rubens-test-1f87d.appspot.com/o/buzz2.jpg?alt=media&token=5fbd71e1-b8e2-4377-953f-3e6ac29bd63c',
    },
    {
      id: '1591798011234595',
      name: 'dog-over-fence.jpg',
      size: 898575,
      mimeType: 'image/jpeg',
      url:
        'https://firebasestorage.googleapis.com/v0/b/rubens-test-1f87d.appspot.com/o/dog-over-fence.jpg?alt=media&token=2dc203ce-d036-48af-90c9-4cc3ff2ff210',
    },
    {
      id: '1591799363496120',
      name: 'whatsapp-logo-11.png',
      size: 75120,
      mimeType: 'image/png',
      url:
        'https://firebasestorage.googleapis.com/v0/b/rubens-test-1f87d.appspot.com/o/whatsapp-logo-11.png?alt=media&token=7eb30a72-07f5-487a-8f46-1768264210f5',
    },
    {
      id: '1591798007155092',
      name: 'yellow-octopus-cool-pictures-1.jpg',
      size: 72670,
      mimeType: 'image/jpeg',
      url:
        'https://firebasestorage.googleapis.com/v0/b/rubens-test-1f87d.appspot.com/o/yellow-octopus-cool-pictures-1.jpg?alt=media&token=4aaec98a-cfa6-4e9f-a228-034a245c71db',
    },
  ],
};

it('Check extensions allowed', () => {
  expect(isAllowedExtension('.png')).toEqual(true);
  expect(isAllowedExtension('.jpg')).toEqual(true);

  expect(isAllowedExtension('.jpged')).toEqual(false);
  expect(isAllowedExtension('.exe')).toEqual(false);
  expect(isAllowedExtension('.com')).toEqual(false);
  expect(isAllowedExtension('.sh')).toEqual(false);
  expect(isAllowedExtension('.bash')).toEqual(false);
});

it('Check mimetypes allowed', () => {
  expect(isAllowedMimeType('image/jpeg')).toEqual(true);
  expect(isAllowedMimeType('image/png')).toEqual(true);

  expect(isAllowedMimeType('application/x-sh')).toEqual(false);
  expect(isAllowedMimeType('application/x-bzip')).toEqual(false);
  expect(isAllowedMimeType('application/x-msdownload')).toEqual(false);
});

it('Test totalSize', () => {
  expect(totalSize(data.images)).toEqual('1mb');
  expect(totalSize(data.images.slice(0, 1))).toEqual('216kb');
});

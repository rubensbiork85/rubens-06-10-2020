import {
  ALLOWED_EXTENSIONS,
  ALLOWED_MIMETYPES,
  ALLOWED_SIZE,
} from '../constants';

import { IImage } from '../types';

export const bytesToSize = (bytes: number): string => {
  if (!bytes || bytes === 0) return '0 bytes';
  const sizes = ['bytes', 'kb', 'mb', 'gb', 'tb'];

  const i = Math.floor(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i)) + sizes[i];
};

export const totalSize = (files: IImage[]): string => {
  if (!files) {
    return '0';
  }
  const total = files.reduce((acc, curr) => {
    return (acc += curr.size);
  }, 0);

  return bytesToSize(total);
};

//check filename extension
export const isAllowedExtension = (ext: string): boolean =>
  ext ? ALLOWED_EXTENSIONS.includes(ext.toLocaleLowerCase()) : false;

//check filename mimetype
export const isAllowedMimeType = (mimeType: string): boolean =>
  mimeType ? ALLOWED_MIMETYPES.includes(mimeType.toLocaleLowerCase()) : false;

//check file size
export const isAllowedSize = (size: number): boolean =>
  size ? size <= ALLOWED_SIZE : false;

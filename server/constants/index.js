module.exports = Object.freeze({
  ALLOWED_EXTENSIONS: ['.png', '.jpg'],
  ALLOWED_MIMETYPES: ['image/png', 'image/jpeg'],
  FILE_LIMITS: {
    fields: 0, //Max number of non-file fields
    files: 1, //Max number fiels
    parts: 1, //file + fields
    fileSize: 10 * 1024 * 1024, // limiting files size to 10 MB
  },
});

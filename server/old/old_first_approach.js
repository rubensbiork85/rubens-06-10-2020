// This implementation was my very first approach
// The endpoint /upload saves the file locally in the server.
// I realized that it brings risk to the server in case of
// any malicious file runs somehow. After it, I switched to the current architeture
// having Front End -> Node Proxy -> Firebase storage

// const fileUpload = require('express-fileupload');

// const fileConfig = fileUpload({
//   useTempFiles: true, //use temporary files instead of memory. avoids memory overflow
//   tempFileDir: '../temp/', //save temp outside of root directory
//   safeFileNames: true, //remove non-alphanumeric characters
//   preserveExtension: 4, //max caracters for the file extension allowed
//   abortOnLimit: true, //abort if file is bigger than the size limit
//   limits: { fileSize: 10 * 1024 * 1024 }, //file size limit
// });

// Endpoint that saves upload file to local folder outside of the server (../uploaded)
// app.post('/upload', fileConfig, (req, res) => {
//   try {
//     console.log('my log' + req.files);
//     if (req.files === null) {
//       res.sendStatus(400).send('No file uploaded.');
//       return;
//     }

//     const file = req.files.file;
//     checkFilenameLength(req.name);
//     checkMimeType(file.mimetype);
//     sanitizeFilename(file.name);
//     const path = `${__dirname}/../uploaded/${file.name}`;

//     file.mv(path, (err) => {
//       if (err) {
//         console.error(err);
//         return res.sendStatus(500).send('Error when saving file');
//       }

//       res.sendStatus(200);
//     });
//   } catch (error) {
//     res.sendStatus(400).send(`Error, could not upload file: ${error}`);
//     return;
//   }
// });

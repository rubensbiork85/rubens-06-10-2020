const express = require('express');
const router = express.Router();
const path = require('path');
const multer = require('multer');
const { Storage } = require('@google-cloud/storage');

//firebase helper
const { getImageInfo } = require('../utils/firebaseHelper');

//filename validations
const {
  sanitizeFilename,
  isAllowedExtension,
  isAllowedMimeType,
} = require('../utils');

const { FILE_LIMITS, ALLOWED_EXTENSIONS } = require('../constants');

// Create new storage instance with Firebase project credentials
const storage = new Storage({
  projectId: process.env.GCLOUD_PROJECT_ID,
  keyFilename: process.env.GCLOUD_APPLICATION_CREDENTIALS,
});

// Create a bucket associated to Firebase storage bucket
const bucket = storage.bucket(process.env.GCLOUD_STORAGE_BUCKET_URL);

//Get images urls from firebase
/*
 * @param  {String} filename (optional for query)
 * @return {Array} data
 */
router.get('/getImages', async (req, res) => {
  try {
    const filename = sanitizeFilename(req.query.filename) || '';
    const [files] = await bucket.getFiles({ prefix: filename });
    const arrPromises = [];

    //loop through all files
    files.forEach((file) => {
      const url = getImageInfo(file);
      arrPromises.push(url);
    });

    //consume list of promises
    const resp = await Promise.all(arrPromises);

    //prevent returning to the UI files that are not allowed
    const images = resp.filter(
      (item) =>
        isAllowedMimeType(item.mimeType) &&
        isAllowedExtension(path.extname(item.name)),
    );

    res.send({ images });
  } catch {
    //Todo: Log Error object for monitoring
    //Avoid showing stacktrace of the errors
    res.status(400).send('Error retriving images.');
  }
});

//Delete a particular image endpoint.
//Params: filename<string>
router.delete('/deleteImage', async (req, res) => {
  try {
    const { filename } = req.query;
    const sanFilename = sanitizeFilename(filename);
    await bucket.file(sanFilename).delete();
    res.sendStatus(200);
  } catch {
    //Todo: Log Error object for monitoring
    res.status(400).send('Image not found.');
  }
});

const uploader = multer({
  //hopefully multer handles super large files well applying the limits defined below
  //then not causing memory overflow
  //and alternative would be to save temporarily in the disk
  storage: multer.memoryStorage(),
  //filter file before hitting the endpoint /uploadImage
  fileFilter: (req, file, callback) => {
    const fileName = sanitizeFilename(file.originalname);
    const filenameExt = path.extname(fileName);
    if (!isAllowedMimeType(file.mimetype) || !isAllowedExtension(filenameExt)) {
      return callback(null, false);
    }
    callback(null, true);
  },
  //set limits of the file like size and number of files
  limits: FILE_LIMITS,
});

//Upload image endpoint to firebase storage
//Param: image (data)
router.post('/uploadImage', uploader.single('file'), async (req, res, next) => {
  try {
    if (!req.file) {
      res
        .status(400)
        .send(
          `Invalid image format. It is only allowed : ${ALLOWED_EXTENSIONS.join()}`,
        );
      return;
    }

    const fileName = sanitizeFilename(req.file.originalname);

    // Create new blob in the bucket referencing the file
    const blob = bucket.file(fileName);

    // Create writable stream and specifying file mimetype
    const blobWriter = blob.createWriteStream({
      metadata: {
        contentType: req.file.mimetype,
      },
    });

    blobWriter.on('error', (err) => next(err));

    blobWriter.on('finish', () => {
      // Assembling public URL for accessing the file via HTTP
      const publicUrl = `https://firebasestorage.googleapis.com/v0/b/${
        bucket.name
      }/o/${encodeURI(blob.name)}?alt=media`;

      // Return the file name and its public URL
      res.status(200).send({ fileName, fileLocation: publicUrl });
    });

    // When there is no more data to be consumed from the stream
    blobWriter.end(req.file.buffer);
  } catch {
    //Todo: Log Error object for monitoring
    res.status(400).send('Error when upload image.');
  }
});

module.exports = router;

require('dotenv').config();
const express = require('express');

//request limit per IP
const rateLimit = require('express-rate-limit');

//safe headers
const helmet = require('helmet');

//log
const logger = require('./utils/logger');

const app = express();
const port = 3001;

//add log middleware
app.use(logger);

//DoS protection: limit each IP to 100 request in 15 mintues
const limiter = rateLimit({
  windowMs: 15 * 60 * 1000,
  max: 100,
});
app.use(limiter);

//add safe headers https://helmetjs.github.io/
app.use(helmet());
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'"],
    },
  }),
);

//hide server info
app.use(helmet.hidePoweredBy({ setTo: '-' }));

//Import endpoints
app.use(require('./routes'));

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});

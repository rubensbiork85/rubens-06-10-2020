const createUuid = require('uuid-v4');
const { tryParseInt } = require('../utils');

// build the downloadURL using the file infomation; adds the download token if it doesn't exists
const getImageInfo = async (file) => {
  const bucket = file.bucket.name;
  const [metadata] = await file.getMetadata();

  let downloadToken;

  if (!metadata.metadata) {
    downloadToken = await setFirebaseMetadata(file);
  } else {
    downloadToken = metadata.metadata.firebaseStorageDownloadTokens.split(
      ',',
    )[0];
  }

  const url = `https://firebasestorage.googleapis.com/v0/b/${bucket}/o/${encodeURIComponent(
    file.name,
  )}?alt=media&token=${downloadToken}`;

  return {
    id: metadata.generation,
    name: metadata.name,
    size: tryParseInt(metadata.size),
    mimeType: metadata.contentType,
    url,
  };
};

//Set metadata to the firebase file
const setFirebaseMetadata = async (file) => {
  const downloadToken = createUuid();
  const metadata = {
    metadata: {
      firebaseStorageDownloadTokens: downloadToken,
    },
  };
  await file.setMetadata(metadata);
  return downloadToken;
};

module.exports = { getImageInfo };

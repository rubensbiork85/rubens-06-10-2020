const sanitize = require('sanitize-filename');
const { ALLOWED_EXTENSIONS, ALLOWED_MIMETYPES } = require('../constants');

module.exports = {
  //remove malicious characters and truncate with utf-8 more info:
  //https://github.com/parshap/node-sanitize-filename/blob/master/index.js
  sanitizeFilename: (filename) => filename && sanitize(filename),

  //check filename extension
  isAllowedExtension: (ext) =>
    ext && ALLOWED_EXTENSIONS.includes(ext.toLocaleLowerCase()),

  //check filename mimetype
  isAllowedMimeType: (mimeType) =>
    mimeType && ALLOWED_MIMETYPES.includes(mimeType.toLocaleLowerCase()),

  //try to parse string to number
  tryParseInt: (value) => {
    let result = 0;
    if (value) {
      if (value.length > 0) {
        if (!isNaN(value)) {
          result = parseInt(value);
        }
      }
    }
    return result;
  },
};

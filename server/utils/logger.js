//based in https://codesource.io/creating-a-logging-middleware-in-expressjs/
const fs = require('fs');

const getActualRequestDurationInMilliseconds = (start) => {
  const NS_PER_SEC = 1e9; // convert to nanoseconds
  const NS_TO_MS = 1e6; // convert to milliseconds
  const diff = process.hrtime(start);
  return (diff[0] * NS_PER_SEC + diff[1]) / NS_TO_MS;
};

const log = (req, res, next) => {
  try {
    const current_datetime = new Date().toISOString().split('|')[0];
    const method = req.method;
    const url = req.url;
    const status = res.statusCode;
    const start = process.hrtime();
    const durationInMilliseconds = getActualRequestDurationInMilliseconds(
      start,
    );
    const log = `[${current_datetime}] ${method}:${url} ${status} ${durationInMilliseconds.toLocaleString()} ms`;

    console.log(log);

    fs.appendFile('./logs/log.log', log + '\n', (err) => {
      if (err) {
        console.log(`Error when saving log: ${err}`);
      }
    });
  } catch (err) {
    console.log(`Error on log: ${err}`);
  }

  next();
};

module.exports = log;
